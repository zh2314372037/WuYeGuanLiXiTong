﻿#pragma checksum "..\..\..\..\PayFees\PayFeesDetails.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "31FB83AD67331EF8407BF578D9D89628"
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.36415
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using 物业自助服务系统.UserControls;


namespace 物业自助服务系统.PayFees {
    
    
    /// <summary>
    /// PayFeesDetails
    /// </summary>
    public partial class PayFeesDetails : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 30 "..\..\..\..\PayFees\PayFeesDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel FeesContent;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\..\PayFees\PayFeesDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal 物业自助服务系统.UserControls.OkButton OkButton;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\..\PayFees\PayFeesDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal 物业自助服务系统.UserControls.BackButton BackButton;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\..\PayFees\PayFeesDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal 物业自助服务系统.UserControls.ExitButton ExitButton;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\..\PayFees\PayFeesDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image TimerImage;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\..\PayFees\PayFeesDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock TimeSurplus;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/物业自助服务系统;component/payfees/payfeesdetails.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\PayFees\PayFeesDetails.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.FeesContent = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 2:
            this.OkButton = ((物业自助服务系统.UserControls.OkButton)(target));
            
            #line 60 "..\..\..\..\PayFees\PayFeesDetails.xaml"
            this.OkButton.AddHandler(System.Windows.Controls.Primitives.ButtonBase.ClickEvent, new System.Windows.RoutedEventHandler(this.OkButton_Click));
            
            #line default
            #line hidden
            return;
            case 3:
            this.BackButton = ((物业自助服务系统.UserControls.BackButton)(target));
            
            #line 62 "..\..\..\..\PayFees\PayFeesDetails.xaml"
            this.BackButton.AddHandler(System.Windows.Controls.Primitives.ButtonBase.ClickEvent, new System.Windows.RoutedEventHandler(this.BackButton_Click));
            
            #line default
            #line hidden
            return;
            case 4:
            this.ExitButton = ((物业自助服务系统.UserControls.ExitButton)(target));
            
            #line 63 "..\..\..\..\PayFees\PayFeesDetails.xaml"
            this.ExitButton.AddHandler(System.Windows.Controls.Primitives.ButtonBase.ClickEvent, new System.Windows.RoutedEventHandler(this.ExitButton_Click));
            
            #line default
            #line hidden
            return;
            case 5:
            this.TimerImage = ((System.Windows.Controls.Image)(target));
            return;
            case 6:
            this.TimeSurplus = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

