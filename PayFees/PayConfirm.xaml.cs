﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using 物业自助服务系统.Utils;

namespace 物业自助服务系统.PayFees
{
    /// <summary>
    /// PayConfirm.xaml 的交互逻辑
    /// </summary>
    public partial class PayConfirm : Page
    {
        public PayConfirm()
        {
            InitializeComponent();
            LoadedData();
            TimerAnimation.Start(this, 60, TimeSurplus, TimerImage);
            TimerAnimation.Start(this, ThrowImage,12);//钞票移动动画
        }
        private void LoadedData()
        {
            PromptContent.Text = "1）系统全面纵览人工智能（AI）的 23 个分支技术。"+
"2）明晰人工智能（AI）下各分支技术的历史发展路径，解读现有瓶颈及未来发展趋势。"+
"3）分析人工智能（AI）下各分支技术在产业中的实际应用情况，评估其在「研究」、「工程」、"+
"「应用」、「社会影响」这四个阶段中所处位置，为计划使用人工智能技术的决策者提供决策参考。"+
"4）为 AI 从业者提供技术趋势参考；为产业方、初学者等提供系统性的技术学习资料。";
        }
        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri(@"\PayFees\PayFeesHandle.xaml", UriKind.Relative));
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri(@"\Home\HomePage.xaml", UriKind.Relative));
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
                this.NavigationService.GoBack();
        }
    }
}
