﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using 物业自助服务系统.Utils;

namespace 物业自助服务系统.PayFees
{
    /// <summary>
    /// PayFeesDone.xaml 的交互逻辑
    /// </summary>
    public partial class PayFeesDone : Page
    {
        public PayFeesDone()
        {
            InitializeComponent();
            TimerAnimation.Start(this, 30, TimeSurplus, TimerImage);
        }
        private void BackButton_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
                this.NavigationService.GoBack();
        }
        private void ExitButton_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new Uri(@"\Home\HomePage.xaml", UriKind.Relative));
        }
    }
}
