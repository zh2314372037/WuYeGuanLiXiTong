﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using 物业自助服务系统.Utils;
using 物业自助服务系统.UserControls;

namespace 物业自助服务系统.HistoryPay
{
    /// <summary>
    /// HistoryPayDetailsPage.xaml 的交互逻辑
    /// </summary>
    public partial class HistoryPayDetailsPage : Page
    {
        public HistoryPayDetailsPage()
        {
            InitializeComponent();
            LoadedData();
            TimerAnimation.Start(this, 60, TimeSurplus, TimerImage);
        }
        private void LoadedData()//加载数据项
        {
            ListRowControl list = null;
            for (int i = 0; i < 7; i++)
            {
                list = new ListRowControl("" + i, "2018-1-" + i, "XXX" + i, "3" + i, "已交费" + i);
                FeesContent.Children.Add(list);
            }
        }
        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri(@"\Home\HomePage.xaml", UriKind.Relative));
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
                this.NavigationService.GoBack();
        }
    }
}
