﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows.Threading;
using System.Windows.Navigation;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Windows.Controls;
using 物业自助服务系统.Home;

namespace 物业自助服务系统.Utils
{
    class TimerAnimation
    {
        /// <summary>
        /// 倒计时动画
        /// </summary>
        /// <param name="obj">当前this</param>
        /// <param name="time">倒计时时间</param>
        /// <param name="textBlock">要改变的TextBlock(可为空)</param>
        /// <param name="image">要改变的Image</param>
        public static void Start(DependencyObject obj,int time,TextBlock textBlock,Image image)
        {
            int i = 0;
            System.Timers.Timer timer = new System.Timers.Timer();
            System.Timers.Timer animation = new System.Timers.Timer();
            timer.EndInit();
            animation.EndInit();
            animation.Interval = 100;//图片刷新速度
            timer.Interval = 1000;
            timer.Start();
            animation.Start();
            timer.Elapsed += new ElapsedEventHandler((sender, e) =>
            {
                obj.Dispatcher.BeginInvoke(new Action(() =>
                {
                    if(textBlock!=null)
                    textBlock.Text = time.ToString();
                    if (time == 0)
                    {
                        if (NavigationService.GetNavigationService(obj) != null)
                            NavigationService.GetNavigationService(obj).Navigate(new Uri("Home/HomePage.xaml",UriKind.Relative));
                    }
                    time--;
                }));
            });
            animation.Elapsed += new ElapsedEventHandler((sender, e) =>
            {
                obj.Dispatcher.BeginInvoke(new Action(() =>
                {
                    i++;
                    image.Source = new BitmapImage(new Uri("/Img/倒计时动画/" + i + ".png", UriKind.Relative));
                    if (i == 12) { i = 0; }
                }));
            });
        }
        /// <summary>
        /// 简单图片动画
        /// </summary>
        /// <param name="obj">当前this</param>
        /// <param name="image">要改变的image</param>
        /// <param name="endIndex">图片文件结束处</param>
        public static void Start(DependencyObject obj,Image image,int endIndex)
        {
            int startIndex = 0;
            System.Timers.Timer animation = new System.Timers.Timer();
            animation.EndInit();
            animation.Interval = 100;//图片刷新速度
            animation.Elapsed += new ElapsedEventHandler((sender, e) =>
            {
                obj.Dispatcher.BeginInvoke(new Action(() =>
                {
                    startIndex++;
                    image.Source = new BitmapImage(new Uri("/Img/投币/money" + startIndex + ".png", UriKind.Relative));
                    if (startIndex == endIndex) { startIndex = 0; }
                }));
            });
            animation.Start();
        }
    }
}
