﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Animation;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows;
using 物业自助服务系统.News;

namespace 物业自助服务系统.Utils
{
    class NewsAnimation
    {
        static DoubleAnimation DA = null;
        static TranslateTransform TT = null;
        /// <summary>
        /// 开始动画
        /// </summary>
        /// <param name="pane">要移动的元素</param>
        /// <param name="startValue">开始值</param>
        /// <param name="endValue">结束值</param>
        public static void StartAnimation(StackPanel pane,double startValue,double endValue)
        {
            DA = new DoubleAnimation();
            TT = new TranslateTransform();
            Duration d = new Duration(TimeSpan.FromSeconds(1));
            DA.From = startValue;
            DA.To = endValue;
            TT.X = 0;
            DA.Duration = d;
            DA.DecelerationRatio = 0.6;
            DA.AccelerationRatio = 0.2;
            pane.RenderTransform = TT;
            TT.BeginAnimation(TranslateTransform.XProperty, DA);
        }
        public static void EndAnimation()
        {
            if (DA != null)
                DA = null;
            if (TT != null)
                TT = null;
        }
    }
}
