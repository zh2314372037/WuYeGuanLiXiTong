﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows;

namespace 物业自助服务系统.Utils
{
    class LogOutput
    {
        /// <summary>
        /// 日志输出
        /// </summary>
        /// <param name="text">要输出的信息</param>
        public static void Output(string text)
        {
            string FilePath = @"D:\log.txt";//日志路径
            try
            {
            re:
                if (!File.Exists(FilePath))
                {
                    File.Create(FilePath);
                }
                else
                {
                    FileInfo info = new FileInfo(FilePath);
                    long fileSize = info.Length;
                    if (fileSize >= 1048576)//判断文件大小，超过1MB就删除
                    {
                        info.Delete();
                        goto re;
                    }
                    info = null;
                }
                using (StreamWriter SW = File.AppendText(FilePath))
                {
                    SW.WriteLine("["+DateTime.Now.ToString()+"]" + text);
                }
            }catch{}
        }
    }
}
