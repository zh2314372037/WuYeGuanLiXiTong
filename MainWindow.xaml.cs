﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Navigation;
using System.Timers;
using System.Windows.Threading;
using 物业自助服务系统.Utils;

namespace 物业自助服务系统
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    ///
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            UpdateTime();
            LogOutput.Output(":软件启动");
        }
        /// <summary>
        /// 更新界面显示的时间
        /// </summary>
        private void UpdateTime()
        {
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 1000;//更新间隔
            timer.Elapsed += new ElapsedEventHandler((sender, e) =>
            {
                this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    TimeDay.Text = DateTime.Now.Year+"年"+DateTime.Now.Month+"月"+DateTime.Now.Day+"日 "+Week();
                    TimeHours.Text =DateTime.Now.Hour+":"+DateTime.Now.Minute+":"+DateTime.Now.Second;
                }));
            });
            timer.Start();
        }
        public string Week()//获取星期几
        {
            string[] weekdays = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
            string week = weekdays[Convert.ToInt32(DateTime.Now.DayOfWeek)];
            return week;
        }
    }
}