﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 物业自助服务系统.Model
{
    /// <summary>
    /// 缴费
    /// </summary>
    class FeesQuery
    {
        public static Int16[] Id { get; set; }//序号
        public static String[] Date{get;set;}//日期
        public static String[] Project { get; set; }//项目
        public static String[] Amount { get; set; }//金额
        public static String[] Static {get;set;}//状态
    }
}
