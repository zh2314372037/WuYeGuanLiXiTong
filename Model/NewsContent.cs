﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace 物业自助服务系统.Model
{
    /// <summary>
    /// 跳转到新闻详情页面才加载数据，为页面导航传递数据的作用
    /// </summary>
    class NewsContent
    {
        public static Image image{get;set;}//当前新闻图片
        public static List<string> content = new List<string>();//标题的索引为0,其余为内容
    }
}
