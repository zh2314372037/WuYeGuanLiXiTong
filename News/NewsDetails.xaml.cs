﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using 物业自助服务系统.Model;
using 物业自助服务系统.Utils;

namespace 物业自助服务系统.News
{
    /// <summary>
    /// NewsDetails.xaml 的交互逻辑
    /// </summary>
    public partial class NewsDetails : Page
    {
        public NewsDetails()
        {
            InitializeComponent();
            TimerAnimation.Start(this, 120, TimeSurplus, TimerImage);//开始计时
            LoadedContent();
        }
        public void LoadedContent()//加载文章内容并显示
        {
            //if (NewsContent.image != null)
            //    DetailsImage.Background = NewsContent.image;
            if (NewsContent.content[0] != null)
            {
                string text1=NewsContent.content[1];
                string text2 = NewsContent.content[1];
                try
                {
                    text1 = text1.Replace("&nbsp;", "");//删除该字符串
                    text1 = text1.Replace("1&mdash;", "");//删除该字符串
                    text1 = text1.Replace("&ldquo;", "");//删除该字符串
                    text1 = text1.Replace("&rdquo;", "");//删除该字符串
                    text2 = text2.Replace("&ldquo;", "");//删除该字符串
                    text2 = text2.Replace("&rdquo;", "");//删除该字符串
                    text2 = text2.Replace("&nbsp;", "");//删除该字符串
                    text2 = text2.Replace("1&mdash;", "");//删除该字符串
                    text1 = text1.Replace(" ", "");//删除该字符串
                    text2 = text2.Replace(" ", "");//删除该字符串
                }
                catch (Exception error) { LogOutput.Output(error.Message); }
                while (true)//删除前面的所有<span xxxxx>
                {
                    if (text1.Contains("\"" + ">"))
                    {
                        int StringLocation1 = text1.IndexOf("\"" + ">");
                        text1 = text1.Remove(0, StringLocation1 + 2);
                    }
                    else if (text2.Contains("\"" + ">"))
                    {
                        int StringLocation2 = text2.IndexOf("\"" + ">");
                        text2 = text2.Remove(0, StringLocation2 + 2);
                    }
                    else
                        break;
                }
                while (true)//删除所有</span>
                {
                    if (text1.Contains("</span>"))
                    {
                        int StringLocation1 = text1.IndexOf("</span>");
                        text1 = text1.Remove(StringLocation1,7);
                    }
                    else if (text2.Contains("</span>"))
                    {
                        int StringLocation2 = text2.IndexOf("</span>");
                        text2 = text2.Remove(StringLocation2,7);
                    }
                    else
                        break;
                }
                if (text1.Length > 124)
                {
                    text1 = text1.Remove(124);
                    text2 = text2.Remove(0,124);
                    DetailsContent2.Text = text2;
                }
                DetailsContent1.Text = "  "+text1;
                DetailsTitle.Text = NewsContent.content[0];//标题存储在0索引处
                NewsContent.content = new List<string>();
                //NewsContent.imageSource = null;
            }
        }
        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri(@"\Home\HomePage.xaml", UriKind.Relative));
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
                this.NavigationService.GoBack();
        }
    }
}
