﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace 物业自助服务系统.UserControls
{
    /// <summary>
    /// ListRowControl.xaml 的交互逻辑
    /// </summary>
    public partial class ListRowControl : UserControl
    {
        /// <summary>
        /// 显示控件
        /// </summary>
        /// <param name="Id">序号</param>
        /// <param name="Date">日期</param>
        /// <param name="Type">项目</param>
        /// <param name="Amount">金额</param>
        /// <param name="Static">状态</param>
        public ListRowControl(string Id,string Date,string Type ,string Amount,string Static)
        {
            InitializeComponent();
            ItemCon item = new ItemCon();
            item.Id = Id;
            item.Date = Date;
            item.Type = Type;
            item.Amount = Amount;
            item.Static = Static;

            Binding bind1=new Binding();
            Binding bind2 = new Binding();
            Binding bind3 = new Binding();
            Binding bind4 = new Binding();
            Binding bind5 = new Binding();
                bind1.Source = item;
                bind2.Source = item;
                bind3.Source = item;
                bind4.Source = item;
                bind5.Source = item;
                bind1.Path = new PropertyPath("Id");
                item1.SetBinding(TextBlock.TextProperty, bind1);
                bind2.Path = new PropertyPath("Date");
                item2.SetBinding(TextBlock.TextProperty, bind2);
                bind3.Path = new PropertyPath("Type");
                item3.SetBinding(TextBlock.TextProperty, bind3);
                bind4.Path = new PropertyPath("Amount");
                item4.SetBinding(TextBlock.TextProperty, bind4);
                bind5.Path = new PropertyPath("Static");
                item5.SetBinding(TextBlock.TextProperty, bind5);
        }
    }
    public class ItemCon :INotifyPropertyChanged
    {
        private string FeesId="序号";
        private string FeesDate = "日期";
        private string FeesType = "项目";
        private string FeesAmount = "金额";
        private string FeesStatic="状态";
        public string Id 
        {
            get { return FeesId; } 
            set { FeesId = value;
            if (PropertyChanged != null)
            {
                this.PropertyChanged.Invoke(this,new PropertyChangedEventArgs("Id"));
            }
          }
        }
        public string Date
        {
            get { return FeesDate;}
            set{FeesDate=value;
            if (PropertyChanged != null)
            {
                this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("Date"));
            }
            }
        }
        public string Type
        {
            get { return FeesType; }
            set
            {
                FeesType = value;
                if (PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("Type"));
                }
            }
        }
        public string Amount
        {
            get { return FeesAmount; }
            set
            {
                FeesAmount = value;
                if (PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("Amount"));
                }
            }
        }
        public string Static
        {
            get { return FeesStatic; }
            set
            {
                FeesStatic = value;
                if (PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("Static"));
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
